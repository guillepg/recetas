package com.guillermo.recetas.service;

import com.guillermo.recetas.entity.TipoIngrediente;
import com.guillermo.recetas.repository.ITipoIngrediente;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service("SERV_TIPO_INGREDIENTE")
public class ServicioTipoIngrediente {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(ServicioTipoIngrediente.class);

    @Autowired
    private ITipoIngrediente repoTipoIngrediente;

    public TipoIngrediente findById(long id){
        return repoTipoIngrediente.findById(id);
    }

    public List<TipoIngrediente> getAll(){
        return repoTipoIngrediente.findAll();
    }
    
    public TipoIngrediente save(TipoIngrediente animal) {
    	return repoTipoIngrediente.save(animal);
    }
}
