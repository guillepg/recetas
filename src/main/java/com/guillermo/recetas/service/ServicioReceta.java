package com.guillermo.recetas.service;

import com.guillermo.recetas.dto.IngredienteRecetaDto;
import com.guillermo.recetas.dto.PasoRecetaDto;
import com.guillermo.recetas.dto.RecetaDto;
import com.guillermo.recetas.entity.Ingrediente;
import com.guillermo.recetas.entity.IngredienteReceta;
import com.guillermo.recetas.entity.PasoReceta;
import com.guillermo.recetas.entity.Receta;
import com.guillermo.recetas.repository.IReceta;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("SERV_RECETA")
public class ServicioReceta {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(ServicioReceta.class);

    @Autowired
    private IReceta repoReceta;
    @Autowired
    private ServicioIngrediente servIngrediente;

    public Receta findById(Integer id){
        return repoReceta.findById(id);
    }
    
    public Receta findByNombre(String nombre){
        return repoReceta.findByNombre(nombre);
    }

    public List<Receta> getAll(){
        return repoReceta.findAll();
    }

    public List<Receta> getList(){
    	List<Receta> recetas = repoReceta.findAll();
    	for(Receta rec : recetas) {
    		rec.setPasos(null);
    		rec.setIngredientes(null);
    	}
        return recetas;
    }
    
    public Receta save(Receta animal) {
    	return repoReceta.save(animal);
    }
    
    public List<String> getRecetasByIngredientes(String ingredientesEntrada){
    	List<String> recetas = new ArrayList<String>();
    	String[] ingredientes = ingredientesEntrada.split(",");
    	for(String ingrediente : ingredientes) {
    		Ingrediente obj = getServIngrediente().findByNombre(ingrediente);
    		if(obj.getRecetas() != null) {
    			for(IngredienteReceta ingRec : obj.getRecetas()) {
    				if(!recetas.contains(ingRec.getNombreReceta())) 
        	    		recetas.add(ingRec.getNombreReceta());
    			}
    		}
    	}
    	return recetas;
    }
    
    public Receta insertReceta(RecetaDto dto) {
    	Receta existente = findByNombre(dto.getNombre());
    	if(existente != null) {
    		// return error ya existe
    	} 
		Receta nuevaReceta = new Receta();
		nuevaReceta.setNombre(dto.getNombre());
		nuevaReceta.setDescripcion(dto.getDescripcion());
		
		Receta recetaGuardada = save(nuevaReceta);
		
		for(PasoRecetaDto pasoDto : dto.getPasos()) {
			PasoReceta paso = new PasoReceta(pasoDto.getOrden(), pasoDto.getTexto(), recetaGuardada);
			recetaGuardada.addPaso(paso);
		}

		for(IngredienteRecetaDto ingredienteDto : dto.getIngredientes()) {
			Ingrediente ingrediente = servIngrediente.findByNombre(ingredienteDto.getNombreIngrediente());
			if(ingrediente == null) 
				ingrediente = servIngrediente.save(new Ingrediente(ingredienteDto.getNombreIngrediente(), null));
			IngredienteReceta ingr = new IngredienteReceta(ingredienteDto.getCantidad(), ingrediente, recetaGuardada);
			recetaGuardada.addIngrediente(ingr);
		}
    	return save(recetaGuardada);
    }
    
    // GETTERS / SETTERS

	public IReceta getRepoReceta() {
		return repoReceta;
	}

	public void setRepoReceta(IReceta repoReceta) {
		this.repoReceta = repoReceta;
	}

	public ServicioIngrediente getServIngrediente() {
		return servIngrediente;
	}

	public void setServIngrediente(ServicioIngrediente servIngrediente) {
		this.servIngrediente = servIngrediente;
	}
}
