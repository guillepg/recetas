package com.guillermo.recetas.service;

import com.guillermo.recetas.entity.Ingrediente;
import com.guillermo.recetas.repository.IIngrediente;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service("SERV_INGREDIENTE")
public class ServicioIngrediente {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(ServicioIngrediente.class);

    @Autowired
    private IIngrediente repoIngrediente;

    public Ingrediente findByNombre(String nombre){
        return repoIngrediente.findByNombre(nombre);
    }

    public List<Ingrediente> getAll(){
        return repoIngrediente.findAll();
    }

    public List<Ingrediente> getNames(){
    	List<Ingrediente> ingredientes = repoIngrediente.findAll();
    	for(Ingrediente ing : ingredientes) {
    		ing.setRecetas(null);
    	}
        return ingredientes;
    }
    
    public Ingrediente save(Ingrediente prop) {
    	return repoIngrediente.save(prop);
    }
}
