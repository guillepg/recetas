package com.guillermo.recetas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootApplication
@EnableAutoConfiguration //EnableConfigurationProperties
@EntityScan(basePackages = {"com.guillermo.recetas.entity"})  // scan JPA entities
public class BahiaApplication {

	private static ConfigurableApplicationContext applicationContext;

	public static void main(String[] args) {		
		BahiaApplication.applicationContext = SpringApplication.run(BahiaApplication.class, args);
	}

}
