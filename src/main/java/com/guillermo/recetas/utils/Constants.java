package com.guillermo.recetas.utils;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author a940
 */
public class Constants {
    
    public interface FORMATOFECHA {
        String DDMMYYYY = "dd-MM-yyyy HH:mm:ss";
        String YYYYMMDD = "yyyy-MM-dd HH:mm:ss";
        String YYYYMMDD_T_HHMMSS_Z = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
        String FORMATO_FECHAS_ENTIDADES = YYYYMMDD_T_HHMMSS_Z;
    }
    public interface TIMEZONE {
        String EUROPA_MADRID = "Europe/Madrid";
    }
}
