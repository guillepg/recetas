package com.guillermo.recetas.utils;

/**
 *
 * @author a940
 */
public class Utils {
    
    
    public static String StackTraceToString(Exception ex) {
        String result = ex.toString() + "\n";
        StackTraceElement[] trace = ex.getStackTrace();
        for (int i = 0; i < trace.length; i++) {
            result += "\t\t" + trace[i].toString() + "\n";
        }
        return result;
    }
    public static String StackTraceToString(Exception ex, Integer lineas) {
        String result = ex.toString() + "\n";
        StackTraceElement[] trace = ex.getStackTrace();
        for (int i = 0; i < trace.length && i < lineas; i++) {
            result += "\t\t" + trace[i].toString() + "\n";
        }
        return result;
    }
/*
	public static DateTime stringToDateTime(String dateStr, String format) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(format);
        return dateStr != null ? formatter.parseDateTime(dateStr) : null;
    }
    
    public static String dateTimeToString(DateTime date, String format) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern(format);
        return date != null ? formatter.print(date) : null;
    }*/
}
