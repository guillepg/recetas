package com.guillermo.recetas.utils;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.PropertySource;


/**
 *
 * @author Guillermo Perez
 */
@WebListener
@PropertySource("classpath:application.properties")
public class ShutdownListener implements ServletContextListener {  

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(ShutdownListener.class);
    
    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("----> Servidor iniciado <----");
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        LOG.info(" ---> /Replegando servidor");
    }
}
