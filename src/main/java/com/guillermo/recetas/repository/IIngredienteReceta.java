package com.guillermo.recetas.repository;

import java.io.Serializable;
import java.util.List;
import com.guillermo.recetas.entity.IngredienteReceta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository("REPO_INGREDIENTE_RECETA")
public interface IIngredienteReceta extends JpaRepository<IngredienteReceta, Serializable>, PagingAndSortingRepository<IngredienteReceta, Serializable> {

    public abstract IngredienteReceta findById(long id);

    public abstract List<IngredienteReceta> findAll();

}
