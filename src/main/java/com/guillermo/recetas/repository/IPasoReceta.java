package com.guillermo.recetas.repository;

import java.io.Serializable;
import java.util.List;
import com.guillermo.recetas.entity.PasoReceta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository("REPO_PASO_RECETA")
public interface IPasoReceta extends JpaRepository<PasoReceta, Serializable>, PagingAndSortingRepository<PasoReceta, Serializable> {

    public abstract PasoReceta findById(long id);

    public abstract List<PasoReceta> findAll();

}
