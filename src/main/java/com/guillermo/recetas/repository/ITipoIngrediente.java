package com.guillermo.recetas.repository;

import java.io.Serializable;
import java.util.List;
import com.guillermo.recetas.entity.TipoIngrediente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository("REPO_TIPO_INGREDIENTE")
public interface ITipoIngrediente extends JpaRepository<TipoIngrediente, Serializable>, PagingAndSortingRepository<TipoIngrediente, Serializable> {

    public abstract TipoIngrediente findById(long id);

    public abstract List<TipoIngrediente> findAll();

}
