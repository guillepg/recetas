package com.guillermo.recetas.repository;

import java.io.Serializable;
import java.util.List;
import com.guillermo.recetas.entity.Ingrediente;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository("REPO_INGREDIENTE")
public interface IIngrediente extends JpaRepository<Ingrediente, Serializable>, PagingAndSortingRepository<Ingrediente, Serializable> {

    public abstract Ingrediente findById(long id);

    public abstract List<Ingrediente> findAll();

    public abstract Ingrediente findByNombre(String nombre);

}
