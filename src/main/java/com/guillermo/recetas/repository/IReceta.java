package com.guillermo.recetas.repository;

import java.io.Serializable;
import java.util.List;
import com.guillermo.recetas.entity.Receta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;


@Repository("REPO_RECETA")
public interface IReceta extends JpaRepository<Receta, Serializable>, PagingAndSortingRepository<Receta, Serializable> {

    public abstract Receta findById(Integer id);

    public abstract Receta findByNombre(String nombre);

    public abstract List<Receta> findAll();

}
