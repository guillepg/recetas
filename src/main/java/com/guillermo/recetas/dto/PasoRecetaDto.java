package com.guillermo.recetas.dto;

import java.io.Serializable;

public class PasoRecetaDto implements Serializable {

	private static final long serialVersionUID = 1L;
	
    private Integer orden;
    private String texto;

    public PasoRecetaDto() { }

    //<editor-fold desc="GETTERS / SETTERS" defaultstate="collapsed">

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}
    //</editor-fold>
}
