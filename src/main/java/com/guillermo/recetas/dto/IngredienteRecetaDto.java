package com.guillermo.recetas.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;

public class IngredienteRecetaDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String cantidad;	
	private String nombreIngrediente;

	public IngredienteRecetaDto() { }

    public String getNombreIngrediente() {
		return nombreIngrediente;
	}

	public void setNombreIngrediente(String nombreIngrediente) {
		this.nombreIngrediente = nombreIngrediente;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
}
