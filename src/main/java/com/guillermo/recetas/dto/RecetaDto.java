package com.guillermo.recetas.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class RecetaDto implements Serializable {

	private static final long serialVersionUID = 1L;

    private Integer id;
    private String nombre;
    private String descripcion;
    private Set<IngredienteRecetaDto> ingredientes = new HashSet<IngredienteRecetaDto>();
    private List<PasoRecetaDto> pasos = new ArrayList<PasoRecetaDto>();

    public RecetaDto() { }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Set<IngredienteRecetaDto> getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(Set<IngredienteRecetaDto> ingredientes) {
		this.ingredientes = ingredientes;
	}

    public List<PasoRecetaDto> getPasos() {
		return pasos;
	}

	public void setPasos(List<PasoRecetaDto> pasos) {
		this.pasos = pasos;
	}

}
