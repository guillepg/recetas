package com.guillermo.recetas;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.guillermo.recetas")
public class SpringConfigurador {
  
}