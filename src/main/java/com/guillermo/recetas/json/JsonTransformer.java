package com.guillermo.recetas.json;

import org.springframework.stereotype.Component;

@Component
public interface JsonTransformer {

    String toJson(Object data);

    <T> T fromJSON(String json, Class<T> clazz);
}
