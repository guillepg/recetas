package com.guillermo.recetas.controller;

import com.guillermo.recetas.entity.Ingrediente;
import com.guillermo.recetas.json.JsonTransformer;
import com.guillermo.recetas.service.ServicioIngrediente;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/ingrediente")
public class IngredienteController {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(IngredienteController.class);

    @Autowired
    ServicioIngrediente service;
    @Autowired
    JsonTransformer jsonTransformer;

    public IngredienteController(ServicioIngrediente serv, JsonTransformer jsonTransformer) {
        this.service = serv;
        this.jsonTransformer = jsonTransformer;
    }

    @GetMapping(value = "/getAll")
    public List<Ingrediente> getAll() {
        return service.getAll();
    }

    @GetMapping(value = "/getNames")
    public List<Ingrediente> getNames() {
        return service.getNames();
    }

    @GetMapping(value = "/findByNombre/{nombre}")
    public ResponseEntity<Ingrediente> findByNombre(@PathVariable(required = true) String nombre) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.findByNombre(nombre));
    }
    
}
