package com.guillermo.recetas.controller;

import com.guillermo.recetas.dto.RecetaDto;
import com.guillermo.recetas.entity.Receta;
import com.guillermo.recetas.json.JsonTransformer;
import com.guillermo.recetas.service.ServicioReceta;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/receta")
public class RecetaController {

    private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(RecetaController.class);

    @Autowired
    ServicioReceta service;
    @Autowired
    JsonTransformer jsonTransformer;

    public RecetaController(ServicioReceta serv, JsonTransformer jsonTransformer) {
        this.service = serv;
        this.jsonTransformer = jsonTransformer;
    }
    
    @CrossOrigin(origins = "null")
    @GetMapping(value = "/getAll")
    public List<Receta> getAll() {
        return service.getAll();
    }
    
    @CrossOrigin(origins = "null")
    @GetMapping(value = "/getNames")
    public List<Receta> getList() {
        return service.getList();
    }

    @CrossOrigin(origins = "null")
    @GetMapping(value = {"/getRecetasByIngredientes/{ingredientes}"})
    public ResponseEntity<List<String>> update(@PathVariable(required = true) String ingredientes) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.getRecetasByIngredientes(ingredientes));
    }

    @CrossOrigin(origins = "null")
    @GetMapping(value = "/findByNombre/{nombre}")
    public ResponseEntity<Receta> findByNombre(@PathVariable(required = true) String nombre) {
    	nombre = nombre.replace("%20", " ");
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.findByNombre(nombre));
    }

    @CrossOrigin(origins = "null")
    @GetMapping(value = "/findById/{id}")
    public ResponseEntity<Receta> findById(@PathVariable(required = true) Integer id) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.findById(id));
    }

    @CrossOrigin(origins = "null")
    @PostMapping(value = "/insertReceta", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Receta> insertReceta(@RequestBody RecetaDto dto) {
        return ResponseEntity.status(HttpStatus.OK)
                .body(service.insertReceta(dto));
    }
    
}
