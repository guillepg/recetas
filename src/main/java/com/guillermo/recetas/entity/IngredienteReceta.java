package com.guillermo.recetas.entity;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.io.Serializable;

@Entity
@Table(name = "INGREDIENTE_RECETA")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "id"})
public class IngredienteReceta implements Serializable {

	private static final long serialVersionUID = 1L;

    @EmbeddedId
    private IngredienteRecetaPK id;
    
	@Column(name = "CANTIDAD")
    private String cantidad;
	
	@Transient
    private String nombreIngrediente;
	
	@Transient
    private String nombreReceta;

	public IngredienteReceta() { }

	public IngredienteReceta(String cantidad, Ingrediente ingrediente, Receta receta) {
		this.cantidad = cantidad;
		this.id.setIngrediente(ingrediente);
		this.id.setReceta(receta);
	}

	public IngredienteRecetaPK getId() {
		return id;
	}

	public void setId(IngredienteRecetaPK id) {
		this.id = id;
	}

    public String getNombreIngrediente() {
    	if(nombreIngrediente == null)
    		this.setNombreIngrediente(id.getIngrediente().getNombre());    	
		return nombreIngrediente;
	}

	public void setNombreIngrediente(String nombreIngrediente) {
		this.nombreIngrediente = nombreIngrediente;
	}

	public String getNombreReceta() {
    	if(nombreReceta == null)
    		this.setNombreReceta(id.getReceta().getNombre());   
		return nombreReceta;
	}

	public void setNombreReceta(String nombreReceta) {
		this.nombreReceta = nombreReceta;
	}

	public String getCantidad() {
		return cantidad;
	}

	public void setCantidad(String cantidad) {
		this.cantidad = cantidad;
	}
}
