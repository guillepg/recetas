package com.guillermo.recetas.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "INGREDIENTE")
public class Ingrediente implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue
    @Column(name = "ID")
    private Integer id;
    @Column(name = "NOMBRE", nullable = false)
    private String nombre;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_TIPO")
    private TipoIngrediente tipo;

    @OneToMany(mappedBy = "id.ingrediente", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<IngredienteReceta> recetas = new ArrayList<IngredienteReceta>();
    
    public Ingrediente() {};
    
    public Ingrediente(String nombre, TipoIngrediente tipo) {
    	this.nombre = nombre;
    	this.tipo = tipo;
    }

    //<editor-fold desc="GETTERS / SETTERS" defaultstate="collapsed">
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public TipoIngrediente getTipo() {
		return tipo;
	}

	public void setTipo(TipoIngrediente tipo) {
		this.tipo = tipo;
	}

	public List<IngredienteReceta> getRecetas() {
		return recetas;
	}

	public void setRecetas(List<IngredienteReceta> recetas) {
		this.recetas = recetas;
	}

	public void addReceta(IngredienteReceta rec) {
		if(this.recetas == null)
			this.recetas = new ArrayList<>();
		this.recetas.add(rec);
	}
	//</editor-fold>
}
