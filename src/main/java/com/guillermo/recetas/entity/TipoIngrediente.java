package com.guillermo.recetas.entity;

import javax.persistence.*;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "TIPO_INGREDIENTE")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler", "ingredientes"})
public class TipoIngrediente implements Serializable{

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue
    @Column(name = "ID")
    private Integer id;
	@Column(name = "NOMBRE")
    private String nombre;

    @OneToMany(mappedBy = "tipo", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<Ingrediente> ingredientes;

    public TipoIngrediente() { }
    
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Ingrediente> getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(List<Ingrediente> ingredientes) {
		this.ingredientes = ingredientes;
	}
}
