package com.guillermo.recetas.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "RECETA")
public class Receta implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue
    @Column(name = "ID")
    private Integer id;
    @Column(name = "NOMBRE", nullable = false)
    private String nombre;
	@Column(name = "DESCRIPCION")
    private String descripcion;

	@OneToMany(mappedBy = "id.receta", fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<IngredienteReceta> ingredientes = new HashSet<IngredienteReceta>();

    @OneToMany(mappedBy = "receta", fetch = FetchType.LAZY, orphanRemoval = true)
    private List<PasoReceta> pasos = new ArrayList<PasoReceta>();

    public Receta() { }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Set<IngredienteReceta> getIngredientes() {
		return ingredientes;
	}

	public void setIngredientes(Set<IngredienteReceta> ingredientes) {
		this.ingredientes = ingredientes;
	}

	public void addIngrediente(IngredienteReceta ingr) {
		if(this.ingredientes == null)
			this.ingredientes = new HashSet<>();
		this.ingredientes.add(ingr);
	}

    public List<PasoReceta> getPasos() {
		return pasos;
	}

	public void setPasos(List<PasoReceta> pasos) {
		this.pasos = pasos;
	}

	public void addPaso(PasoReceta paso) {
		if(this.pasos == null)
			this.pasos = new ArrayList<>();
		this.pasos.add(paso);
	}

}
