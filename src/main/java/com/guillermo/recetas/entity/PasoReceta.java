package com.guillermo.recetas.entity;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "PASO_RECETA")
public class PasoReceta implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue
    @Column(name = "ID")
    private Integer id;
    @Column(name = "ORDEN", nullable = false)
    private Integer orden;
    @Column(name = "TEXTO", nullable = false)
    private String texto;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ID_RECETA")
    private Receta receta;

    public PasoReceta() { }

    public PasoReceta(Integer orden, String texto, Receta receta) {
    	this.orden = orden;
    	this.texto = texto;
    	this.receta = receta;
    }

    //<editor-fold desc="GETTERS / SETTERS" defaultstate="collapsed">
    public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}

	public String getTexto() {
		return texto;
	}

	public void setTexto(String texto) {
		this.texto = texto;
	}

	/*public Receta getReceta() {
		return receta;
	}

	public void setReceta(Receta receta) {
		this.receta = receta;
	}*/
    //</editor-fold>
}
