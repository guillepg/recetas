-- CREATE SCHEMA recetas;
USE recetas;
/*DROP TABLE `ingrediente_receta`;
DROP TABLE `paso_receta`;
DROP TABLE `receta`;
DROP TABLE `ingrediente`;
DROP TABLE `tipo_ingrediente`;*/

CREATE TABLE `tipo_ingrediente` (
	`id` int NOT NULL PRIMARY KEY AUTO_INCREMENT,
	`nombre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `ingrediente` (
	`id` int PRIMARY KEY AUTO_INCREMENT,
	`nombre` varchar(255) NOT NULL,
	`id_tipo` int DEFAULT NULL,
	KEY `key_ingrediente` (`id_tipo`),
	CONSTRAINT `fk_ingrediente` FOREIGN KEY (`id_tipo`) REFERENCES `tipo_ingrediente` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `receta` (
	`id` int PRIMARY KEY AUTO_INCREMENT,
	`descripcion` varchar(255) DEFAULT NULL,
	`nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `ingrediente_receta` (
	`id_ingrediente` int NOT NULL,
	`id_receta` int NOT NULL,
	`cantidad` varchar(255) DEFAULT NULL,
	PRIMARY KEY (`id_ingrediente`,`id_receta`),
	KEY `key_ingrediente_receta_1` (`id_ingrediente`),
	KEY `key_ingrediente_receta_2` (`id_receta`),
	CONSTRAINT `fk_ingrediente_receta_1` FOREIGN KEY (`id_ingrediente`) REFERENCES `ingrediente` (`id`),
	CONSTRAINT `fk_ingrediente_receta_2` FOREIGN KEY (`id_receta`) REFERENCES `receta` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `paso_receta` (
	`id` int PRIMARY KEY AUTO_INCREMENT,
	`orden` int NOT NULL,
	`texto` varchar(255) NOT NULL,
	`id_receta` int NOT NULL,
	CONSTRAINT `fk_paso_receta` FOREIGN KEY (`id_receta`) REFERENCES `receta` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*SHOW CREATE TABLE tipo_ingrediente;
SHOW CREATE TABLE ingrediente;
SHOW CREATE TABLE receta;
SHOW CREATE TABLE ingrediente_receta;
SHOW CREATE TABLE paso_receta;*/