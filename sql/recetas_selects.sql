USE recetas;

SELECT * FROM tipo_ingrediente;
SELECT * FROM ingrediente;
SELECT * FROM receta;
SELECT * FROM ingrediente_receta;
SELECT * FROM paso_receta;

-- Ingredientes
SELECT rec.nombre, ing.nombre, ingrec.cantidad FROM ingrediente_receta ingrec
	JOIN receta rec ON rec.id = ingrec.id_receta
    JOIN ingrediente ing ON ing.id = ingrec.id_ingrediente
ORDER BY rec.nombre ASC;
-- Pasos
SELECT rec.nombre, pasorec.orden, pasorec.texto FROM paso_receta pasorec
	JOIN receta rec ON rec.id = pasorec.id_receta
ORDER BY rec.nombre ASC, pasorec.orden ASC;