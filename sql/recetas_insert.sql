USE recetas;

INSERT INTO tipo_ingrediente (nombre) VALUES 
	('Harinas'), ('Huevo'), ('Aceites'), ('Quesos'), 
    ('Bollería'), ('Desayunos'), ('Hortalizas'), ('Carnes'), 
    ('Condimento'), ('Bebidas'), ('Básicos'), ('Lácteos'), ('Hongos');

INSERT INTO ingrediente (nombre, id_tipo) VALUES 
	('Harina de fuerza', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Harinas')), 
    ('Harina de trigo', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Harinas')), 
    ('Harina integral', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Harinas')),
	('Sal', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Básicos')), 
    ('Azúcar', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Básicos')),
    ('Agua', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Básicos')),
	('Aceite de oliva', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Aceites')), 
	('Aceite de girasol', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Aceites')),
    ('Levadura fresca / Panadero', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Harinas')),
    ('Clara de huevo', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Huevo')),
    ('Yema de huevo', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Huevo')),
    ('Huevo', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Huevo')),
    ('Queso mascarpone', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Quesos')),
    ('Leche evaporada', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Lácteos')),
    ('Bizcocho de soletilla', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Desayunos')),
    ('Cacao', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Desayunos')),
    ('Café', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Desayunos')),
    ('Calabaza naranja', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Hortalizas')),
    ('Zanahoria', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Hortalizas')),
    ('Cebolla', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Hortalizas')),
    ('Puerro', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Hortalizas')),
    ('Patatas', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Hortalizas')),
    ('Pechuga de pollo', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Carnes')),
    ('Lomo', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Carnes')),
    ('Ajo en polvo', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Condimento')),
    ('Pimentón', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Condimento')),
    ('Perejil', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Condimento')),
    ('Orégano', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Condimento')),
    ('Cerveza', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Bebidas')),
	('Champiñones', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Hongos')),
	('Leche', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Lácteos')),
	('Mantequilla', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Lácteos')),
	('Pimienta', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Condimento')),
	('Nuez moscada', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Condimento')),
	('Nata cocinar', (SELECT ID FROM tipo_ingrediente WHERE NOMBRE = 'Lácteos'));

-- HOGAZA
INSERT INTO receta (nombre, descripcion) VALUES ('Hogaza', 'Para hacer tostis');
INSERT INTO ingrediente_receta (id_receta, id_ingrediente, cantidad) VALUES 
	((SELECT ID FROM receta WHERE nombre = 'Hogaza'), (SELECT ID FROM ingrediente WHERE nombre = 'Harina de Fuerza'), '500 gr'),
	((SELECT ID FROM receta WHERE nombre = 'Hogaza'), (SELECT ID FROM ingrediente WHERE nombre = 'Sal'), '2 cucharadas'),
	((SELECT ID FROM receta WHERE nombre = 'Hogaza'), (SELECT ID FROM ingrediente WHERE nombre = 'Aceite de oliva'), '15 ml'),
	((SELECT ID FROM receta WHERE nombre = 'Hogaza'), (SELECT ID FROM ingrediente WHERE nombre = 'Levadura fresca / Panadero'), '1 bloque / 1 sobre'),
	((SELECT ID FROM receta WHERE nombre = 'Hogaza'), (SELECT ID FROM ingrediente WHERE nombre = 'Agua'), '275 ml');    
INSERT INTO paso_receta (id_receta, orden, texto) VALUES 
	((SELECT ID FROM receta WHERE nombre = 'Hogaza'), 1, 'Echar agua y velocidad 1, 37º y 2 min'), 
    ((SELECT ID FROM receta WHERE nombre = 'Hogaza'), 2, 'Si usas levadura fresca, ahora échala y da otros 30 segundos con todo igual. Si es de panadero, echala en el siguiente paso'), 
    ((SELECT ID FROM receta WHERE nombre = 'Hogaza'), 3, 'Echar el resto de ingredientes y velocidad 2-3, 5 min'), 
    ((SELECT ID FROM receta WHERE nombre = 'Hogaza'), 4, 'Hacer una bola y dejar reposar 30 min dentro de la mambo'), 
    ((SELECT ID FROM receta WHERE nombre = 'Hogaza'), 5, 'Sacar, amasar para quitar aire, hacer el dibujo y dejarlo otros 30 min a 50º con el horno apagado y tapado con un trapo.'), 
    ((SELECT ID FROM receta WHERE nombre = 'Hogaza'), 6, 'Hornear 30 min a 225º');
    
-- TIRAMISÚ
INSERT INTO receta (nombre, descripcion) VALUES ('Tiramisú', 'Pa contentar a la suegra');
INSERT INTO ingrediente_receta (id_receta, id_ingrediente, cantidad) VALUES 
	((SELECT ID FROM receta WHERE nombre = 'Tiramisú'), (SELECT ID FROM ingrediente WHERE nombre = 'Clara de huevo'), '2'),
	((SELECT ID FROM receta WHERE nombre = 'Tiramisú'), (SELECT ID FROM ingrediente WHERE nombre = 'Yema de huevo'), '4'),
	((SELECT ID FROM receta WHERE nombre = 'Tiramisú'), (SELECT ID FROM ingrediente WHERE nombre = 'Azúcar'), '100gr'),
	((SELECT ID FROM receta WHERE nombre = 'Tiramisú'), (SELECT ID FROM ingrediente WHERE nombre = 'Queso mascarpone'), '400gr'),
	((SELECT ID FROM receta WHERE nombre = 'Tiramisú'), (SELECT ID FROM ingrediente WHERE nombre = 'Bizcocho de soletilla'), 'Alrededor de un paquete'),
	((SELECT ID FROM receta WHERE nombre = 'Tiramisú'), (SELECT ID FROM ingrediente WHERE nombre = 'Cacao'), 'Al gusto'),
	((SELECT ID FROM receta WHERE nombre = 'Tiramisú'), (SELECT ID FROM ingrediente WHERE nombre = 'Café'), 'Unas 4 capsulas');
INSERT INTO paso_receta (id_receta, orden, texto) VALUES 
	((SELECT ID FROM receta WHERE nombre = 'Tiramisú'), 1, 'Preparar café y reservarlo en un bol.'),
	((SELECT ID FROM receta WHERE nombre = 'Tiramisú'), 2, 'Montar claras'),
	((SELECT ID FROM receta WHERE nombre = 'Tiramisú'), 3, 'Batir yemas con azucar y cuando salga espuma, añadir el mascarpone'),
	((SELECT ID FROM receta WHERE nombre = 'Tiramisú'), 4, 'Cuando ya esté todo mezclado, añadir las claras montadas y envolverlas a mano para que se integren.'),
	((SELECT ID FROM receta WHERE nombre = 'Tiramisú'), 5, 'Untamos los bizcochos en café y ponemos en bandeja, capa de crema, capa de bizcocho, capa de crema y cacao espolvoreado');
    
-- Puré de calabaza
INSERT INTO receta (nombre, descripcion) VALUES ('Puré de calabaza', 'Pa contentar al suegro');
INSERT INTO ingrediente_receta (id_receta, id_ingrediente, cantidad) VALUES 
	((SELECT ID FROM receta WHERE nombre = 'Puré de calabaza'), (SELECT ID FROM ingrediente WHERE nombre = 'Calabaza naranja'), '500 gr'),
	((SELECT ID FROM receta WHERE nombre = 'Puré de calabaza'), (SELECT ID FROM ingrediente WHERE nombre = 'Zanahoria'), '100 gr'),
	((SELECT ID FROM receta WHERE nombre = 'Puré de calabaza'), (SELECT ID FROM ingrediente WHERE nombre = 'Cebolla'), '100 gr'),
	((SELECT ID FROM receta WHERE nombre = 'Puré de calabaza'), (SELECT ID FROM ingrediente WHERE nombre = 'Puerro'), '100 gr'),
	((SELECT ID FROM receta WHERE nombre = 'Puré de calabaza'), (SELECT ID FROM ingrediente WHERE nombre = 'Aceite de oliva'), '50 ml'),
	((SELECT ID FROM receta WHERE nombre = 'Puré de calabaza'), (SELECT ID FROM ingrediente WHERE nombre = 'Leche evaporada'), '200 ml'),
	((SELECT ID FROM receta WHERE nombre = 'Puré de calabaza'), (SELECT ID FROM ingrediente WHERE nombre = 'Agua'), '500 ml');
INSERT INTO paso_receta (id_receta, orden, texto) VALUES 
	((SELECT ID FROM receta WHERE nombre = 'Puré de calabaza'), 1, 'Echar zanahora, puerro y cebolla y dar golpes de turbo.'),
	((SELECT ID FROM receta WHERE nombre = 'Puré de calabaza'), 2, 'Echar aceite y 6 min, 100º, velocidad 2, potencia 8.'),
	((SELECT ID FROM receta WHERE nombre = 'Puré de calabaza'), 3, 'Añadir calabaza a trozos y agua. Y 30 min a 100º, velocidad 2 y potencia 8.'),
	((SELECT ID FROM receta WHERE nombre = 'Puré de calabaza'), 4, 'Añadir leche evaporada, sal y pimienta y dejar que se temple.'),
	((SELECT ID FROM receta WHERE nombre = 'Puré de calabaza'), 5, '2 minutos, velocidad progresiva 6-8.');
    
-- Pechugas con cerveza
INSERT INTO receta (nombre, descripcion) VALUES ('Pechugas con cerveza', 'Cena');
INSERT INTO ingrediente_receta (id_receta, id_ingrediente, cantidad) VALUES 
	((SELECT ID FROM receta WHERE nombre = 'Pechugas con cerveza'), (SELECT ID FROM ingrediente WHERE nombre = 'Pechuga de pollo'), 'Cortada a tiras'),
	((SELECT ID FROM receta WHERE nombre = 'Pechugas con cerveza'), (SELECT ID FROM ingrediente WHERE nombre = 'Harina de trigo'), ''),
	((SELECT ID FROM receta WHERE nombre = 'Pechugas con cerveza'), (SELECT ID FROM ingrediente WHERE nombre = 'Ajo en polvo'), 'Al gusto'),
	((SELECT ID FROM receta WHERE nombre = 'Pechugas con cerveza'), (SELECT ID FROM ingrediente WHERE nombre = 'Pimentón'), 'Al gusto'),
	((SELECT ID FROM receta WHERE nombre = 'Pechugas con cerveza'), (SELECT ID FROM ingrediente WHERE nombre = 'Perejil'), 'Al gusto'),
	((SELECT ID FROM receta WHERE nombre = 'Pechugas con cerveza'), (SELECT ID FROM ingrediente WHERE nombre = 'Orégano'), 'Al gusto'),
	((SELECT ID FROM receta WHERE nombre = 'Pechugas con cerveza'), (SELECT ID FROM ingrediente WHERE nombre = 'Cerveza'), 'Media lata');
INSERT INTO paso_receta (id_receta, orden, texto) VALUES 
	((SELECT ID FROM receta WHERE nombre = 'Pechugas con cerveza'), 1, 'Mezclar todas las especias con la harina y el pimentón.'),
	((SELECT ID FROM receta WHERE nombre = 'Pechugas con cerveza'), 2, 'Echar la cerveza, mezclar y hacer que se quede una masa uniforme. Dejar reposar.'),
	((SELECT ID FROM receta WHERE nombre = 'Pechugas con cerveza'), 3, 'Rebozar la pechuga e inmediatamente llevar al aceite muy caliente.');
    
-- Lomo con cebolla
INSERT INTO receta (nombre, descripcion) VALUES ('Lomo con cebolla', 'Segundo plato');
INSERT INTO ingrediente_receta (id_receta, id_ingrediente, cantidad) VALUES 
	((SELECT ID FROM receta WHERE nombre = 'Lomo con cebolla'), (SELECT ID FROM ingrediente WHERE nombre = 'Lomo'), '4-6 filetes'),
	((SELECT ID FROM receta WHERE nombre = 'Lomo con cebolla'), (SELECT ID FROM ingrediente WHERE nombre = 'Cebolla'), 'Media'),
	((SELECT ID FROM receta WHERE nombre = 'Lomo con cebolla'), (SELECT ID FROM ingrediente WHERE nombre = 'Harina de trigo'), 'Para rebozar');
INSERT INTO paso_receta (id_receta, orden, texto) VALUES 
	((SELECT ID FROM receta WHERE nombre = 'Lomo con cebolla'), 1, 'Cortar cebolla a laminas y pocharla.'),
	((SELECT ID FROM receta WHERE nombre = 'Lomo con cebolla'), 2, 'Rebozar solo harina la carne y echar a la sopera.'),
	((SELECT ID FROM receta WHERE nombre = 'Lomo con cebolla'), 3, 'Echar vino blanco a la mezcla y bajar fuego. Darle la vuelta de vez en cuando para que no se pegue.');
    
-- Tortilla de patata
INSERT INTO receta (nombre, descripcion) VALUES ('Tortilla de patata', 'Cena');
INSERT INTO ingrediente_receta (id_receta, id_ingrediente, cantidad) VALUES 
	((SELECT ID FROM receta WHERE nombre = 'Tortilla de patata'), (SELECT ID FROM ingrediente WHERE nombre = 'Patatas'), '4'),
	((SELECT ID FROM receta WHERE nombre = 'Tortilla de patata'), (SELECT ID FROM ingrediente WHERE nombre = 'Cebolla'), 'Media'),
	((SELECT ID FROM receta WHERE nombre = 'Tortilla de patata'), (SELECT ID FROM ingrediente WHERE nombre = 'Huevo'), '5');
INSERT INTO paso_receta (id_receta, orden, texto) VALUES 
	((SELECT ID FROM receta WHERE nombre = 'Tortilla de patata'), 1, 'Cortar patatas con laminadora.'),
	((SELECT ID FROM receta WHERE nombre = 'Tortilla de patata'), 2, 'Cortar cebolla con picadora.'),
	((SELECT ID FROM receta WHERE nombre = 'Tortilla de patata'), 3, 'Pochar todo a fuego medio. Cuando esté todo pochado, verter en un bol donde esté el huevo batido y mezclar.'),
	((SELECT ID FROM receta WHERE nombre = 'Tortilla de patata'), 4, 'Volver a echar a la sarten y cocinar por ambas partes.');

-- Crema de champiñones
INSERT INTO receta (nombre, descripcion) VALUES ('Crema de champiñones', 'Para 4 personas');
INSERT INTO paso_receta (id_receta, orden, texto) VALUES 
	((SELECT ID FROM receta WHERE nombre = 'Crema de champiñones'), 1, 'Echar cebolla y poner turbo'),
	((SELECT ID FROM receta WHERE nombre = 'Crema de champiñones'), 2, 'Echar aceite, la mantequilla y los champis'),
	((SELECT ID FROM receta WHERE nombre = 'Crema de champiñones'), 3, 'Sofreir 7 minutos, vel 1, 100º y potencia 8 con cubilete medio abierto'),
	((SELECT ID FROM receta WHERE nombre = 'Crema de champiñones'), 4, 'Añadir patata cortada en trozos, leche, sal, pimienta y agua o caldo templado'),
	((SELECT ID FROM receta WHERE nombre = 'Crema de champiñones'), 5, 'Cocinar durante 18min a vel 1, 100º y potencia 6'),
	((SELECT ID FROM receta WHERE nombre = 'Crema de champiñones'), 6, 'Comprobar que la patata esté cocinada y esperar que se temple un poco'),
	((SELECT ID FROM receta WHERE nombre = 'Crema de champiñones'), 7, 'Triturar 1 minuto con velocidad 6-8');
INSERT INTO ingrediente_receta (id_receta, id_ingrediente, cantidad) VALUES 
	((SELECT ID FROM receta WHERE nombre = 'Crema de champiñones'), (SELECT ID FROM ingrediente WHERE nombre = 'Champiñones'), '200 gr crema + 40 decoración'),
	((SELECT ID FROM receta WHERE nombre = 'Crema de champiñones'), (SELECT ID FROM ingrediente WHERE nombre = 'Patatas'), '2'),
	((SELECT ID FROM receta WHERE nombre = 'Crema de champiñones'), (SELECT ID FROM ingrediente WHERE nombre = 'Cebolla'), 'Media (90 gr)'),
	((SELECT ID FROM receta WHERE nombre = 'Crema de champiñones'), (SELECT ID FROM ingrediente WHERE nombre = 'Leche'), '250 ml'),
	((SELECT ID FROM receta WHERE nombre = 'Crema de champiñones'), (SELECT ID FROM ingrediente WHERE nombre = 'Mantequilla'), '25 gr'),
	((SELECT ID FROM receta WHERE nombre = 'Crema de champiñones'), (SELECT ID FROM ingrediente WHERE nombre = 'Aceite de oliva'), '25 ml'),
	((SELECT ID FROM receta WHERE nombre = 'Crema de champiñones'), (SELECT ID FROM ingrediente WHERE nombre = 'Sal'), 'Al gusto'),
	((SELECT ID FROM receta WHERE nombre = 'Crema de champiñones'), (SELECT ID FROM ingrediente WHERE nombre = 'Pimienta'), 'Al gusto'),
	((SELECT ID FROM receta WHERE nombre = 'Crema de champiñones'), (SELECT ID FROM ingrediente WHERE nombre = 'Nuez moscada'), 'Pizca'),
	((SELECT ID FROM receta WHERE nombre = 'Crema de champiñones'), (SELECT ID FROM ingrediente WHERE nombre = 'Agua'), '500 ml (o caldo de pollo)'),
	((SELECT ID FROM receta WHERE nombre = 'Crema de champiñones'), (SELECT ID FROM ingrediente WHERE nombre = 'Nata cocinar'), '100 ml (opcional)');

-- Croquetas de jamón
INSERT INTO receta (nombre, descripcion) VALUES ('Croquetas de jamón', '');
INSERT INTO paso_receta (id_receta, orden, texto) VALUES 
	((SELECT ID FROM receta WHERE nombre = 'Croquetas de jamón'), 1, 'Modo Turbo en la mambo con la cebolla'),
	((SELECT ID FROM receta WHERE nombre = 'Croquetas de jamón'), 2, 'Echar aceite y cebolla. Triturar a 30 segundos a velocidad 6'),
	((SELECT ID FROM receta WHERE nombre = 'Croquetas de jamón'), 3, 'Echar jamón y sofreir: velocidad 2, 8 minutos, 100ºC, potencia 10.'),
	((SELECT ID FROM receta WHERE nombre = 'Croquetas de jamón'), 4, 'Añadir harina y 3 minutos a velocidad 1, 100ºC, potencia 4'),
	((SELECT ID FROM receta WHERE nombre = 'Croquetas de jamón'), 5, 'Añadir leche, sal y pimienta. 20 segundos a velocidad 6.'),
	((SELECT ID FROM receta WHERE nombre = 'Croquetas de jamón'), 6, '14 minutos, velocidada 4, 100ºC, potencia 3');
INSERT INTO ingrediente_receta (id_receta, id_ingrediente, cantidad) VALUES 
	((SELECT ID FROM receta WHERE nombre = 'Croquetas de jamón'), (SELECT ID FROM ingrediente WHERE nombre = 'Cebolla'), '1'),
	((SELECT ID FROM receta WHERE nombre = 'Croquetas de jamón'), (SELECT ID FROM ingrediente WHERE nombre = 'Aceite de oliva'), '50 ml o algo más'),
	((SELECT ID FROM receta WHERE nombre = 'Croquetas de jamón'), (SELECT ID FROM ingrediente WHERE nombre = 'Leche'), '800 ml'),
	((SELECT ID FROM receta WHERE nombre = 'Croquetas de jamón'), (SELECT ID FROM ingrediente WHERE nombre = 'Harina de trigo'), '170g'),
	((SELECT ID FROM receta WHERE nombre = 'Croquetas de jamón'), (SELECT ID FROM ingrediente WHERE nombre = 'Sal'), ''),
	((SELECT ID FROM receta WHERE nombre = 'Croquetas de jamón'), (SELECT ID FROM ingrediente WHERE nombre = 'Pimienta'), '');