'use strict'
const masterUrl = "http://192.168.1.130:7070/recetas";

var allRecetasJson = null;
init();
const switcher = document.querySelector('.btn');

function init(){
    var divReceta = document.getElementById("datosReceta");
    divReceta.style.display = "none";

    console.log("Buscando todas las recetas:", masterUrl + "/receta/getNames");
    var getNames = httpGet(masterUrl + "/receta/getNames");
    console.log("Resultado:", getNames);
    globalThis.allRecetasJson = JSON.parse(getNames);

    var listadoRecetas = document.getElementById("listadoRecetas");
    for (var keyRecetas in globalThis.allRecetasJson) {
        var nuevaReceta = document.createElement('li');
        nuevaReceta.className = "collapsible";
        var textoReceta = globalThis.allRecetasJson[keyRecetas].id + ': ' + globalThis.allRecetasJson[keyRecetas].nombre;
        nuevaReceta.appendChild(document.createTextNode(textoReceta));
        nuevaReceta.addEventListener("click", function() {            
            buscarPorId(this.textContent);
        });
        listadoRecetas.appendChild(nuevaReceta);
    }
}

switcher.addEventListener('click', function() {
    var textoBusqueda = document.getElementById("receta").value;
    
    if(textoBusqueda){
        console.log("Receta introducida: " + textoBusqueda);
        buscar(textoBusqueda);
    } else {
        alert("No se ha introducido ninguna receta para buscar.");
    }
    
});

function buscar(receta){    
    var id = 0;
    console.log("Buscando receta " + receta);
    console.log("globalThis.allRecetasJson " + globalThis.allRecetasJson);
    for (var keyRecetas in globalThis.allRecetasJson) {
        console.log("Comparando " + receta + " con " + globalThis.allRecetasJson[keyRecetas].nombre);
        if(globalThis.allRecetasJson[keyRecetas].nombre === receta)
            id = globalThis.allRecetasJson[keyRecetas].id;
    }
    if(id <= 0){
        alert("No se ha encontrado la receta " + receta);
        return;
    }
    console.log(masterUrl + "/receta/findById/"+ id);
    var resultado = httpGet(masterUrl + "/receta/findById/"+ id);    
    cambiarContenidoReceta(resultado);
}

function buscarPorId(receta){    
    var id = receta.substring(0, receta.indexOf(':'));
    console.log("Buscando receta " + receta + " con ID " + id);

    console.log(masterUrl + "/receta/findById/"+ id);
    var resultado = httpGet(masterUrl + "/receta/findById/"+ id);
    cambiarContenidoReceta(resultado);
}

function cambiarContenidoReceta(resultado){
    var divReceta = document.getElementById("datosReceta");
    if(resultado){
        divReceta.style.display = "block";
        console.log(resultado);
    } else {
        divReceta.style.display = "none";
        alert("No se ha encontrado ninguna receta con el nombre " + receta);
        return;
    }
    var titulo = JSON.parse(resultado).nombre;
    if(JSON.parse(resultado).descripcion.length > 0){
        titulo = titulo + " (" + JSON.parse(resultado).descripcion + ")"
    }
    document.getElementById("descripcion").textContent = titulo;
    var ingredientes = JSON.parse(resultado).ingredientes;
    var listaIngredientes = document.getElementById('ingredientes');
    listaIngredientes.textContent = '';
    for (var keyIngr in ingredientes) {
        var textoIngrediente = ingredientes[keyIngr].nombreIngrediente;
        if(ingredientes[keyIngr].cantidad.length > 0)
            textoIngrediente = textoIngrediente + " (" + ingredientes[keyIngr].cantidad + ")";
        var nuevoIngrediente = document.createElement('li');
        nuevoIngrediente.appendChild(document.createTextNode(textoIngrediente));
        listaIngredientes.appendChild(nuevoIngrediente);
    }
    var pasos = JSON.parse(resultado).pasos;
    var listaPasos = document.getElementById('pasos');
    listaPasos.textContent = '';
    for (var keyPaso in pasos) {
        var nuevoPaso = document.createElement('li');
        nuevoPaso.appendChild(document.createTextNode(pasos[keyPaso].orden + ": " + pasos[keyPaso].texto));
        listaPasos.appendChild(nuevoPaso);
    }
}
function httpGet(theUrl){
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open( "GET", theUrl, false ); // false for synchronous request
    xmlHttp.send( null );
    return xmlHttp.responseText;
}